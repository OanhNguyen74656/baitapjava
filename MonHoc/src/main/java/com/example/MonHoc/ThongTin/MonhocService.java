package com.example.MonHoc.ThongTin;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.plaf.basic.BasicComboPopup.ListDataHandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vimaru.fit.ttm.introspring.introspring.student.String;
import vn.vimaru.fit.ttm.introspring.introspring.student.Student;
import vn.vimaru.fit.ttm.introspring.introspring.student.StudentRepository;

@Service

public class MonhocService {
	 @Autowired
	  MonhocRepository mhRepository;

	  // Lấy thông tin môn học có mã môn hoc tương ứng
	  public Optional<MonHoc> getMonHoc(String mmh) {
	    return mhRepository.findById(mmh);
	  }
	// Tìm kiếm môn học theo tên
	  public List<Monhoct> findMonhoctByName(String name) {
	    List<Monhoc> monhoc = new ArrayList<>();
	    mhRepository.findByName(name).forEach(monhoc::add);
	    return monhoc ;
	  }
    //Tìm  kiếm môn học theo bộ môn phụ trách
	  public List<Monhoc> findMonHocByName(String BMPT) {
		  List<Monhoc> monhoc = new ArrayList<>();
		    mhRepository.findByBMPT(name).forEach(monhoc::add);
		    return monhoc ;
	  }
	// Tạo thêm một môn học
	  public void createMonHoc(String mmh, Monhoc mh) {
	    mh.setMmh(mmh);
	    mhRepository.save(mh);
	  }
	// Cập nhật thông tin môn học
	  public void updateMonHoc(String mmh, Monhoc mh) {
	    mh.setMmh(mmh);
	    mhRepository.save(mh);
	  }
	//Xóa thông tin
	  public void DeleteMonHoc(String mmh, MonHoc mh) {
	    mh.setMmh(mh);
	    mhRepository.save(mh);
	  }


}
