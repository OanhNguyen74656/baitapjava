package com.example.MonHoc.ThongTin;
import java.util.List;

import org.springframework.data.repository.CrudRepository;


	public interface MonHocRepository extends CrudRepository<MonHoc, String> {
		  
		  // findBy<Tên trường dữ liệu>
		  // Tìm kiếm tất cả các học sinh có tên tương ứng
		  List<MonHoc> findByName(String name);


}
