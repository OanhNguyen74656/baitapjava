package com.example.MonHoc.ThongTin;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vn.vimaru.fit.ttm.introspring.introspring.student.String;
import vn.vimaru.fit.ttm.introspring.introspring.student.Student;
import vn.vimaru.fit.ttm.introspring.introspring.student.StudentService;

@RestController

public class MonHocController {
	@Autowired
	  
	MonHocService monhocService;

	  @RequestMapping(method = RequestMethod.GET, value = "/monhoc/{mmh}")
	  public Optional<Student> getMonHocInfo(@PathVariable String mmh) {
		 
	    return monhocService.getStudent(mmh);
	  }

	  @RequestMapping(value="/monhoc/{mmh}", method=RequestMethod.PUT)
	  public void updateMonHoc(@PathVariable String mmh, @RequestBody MonHoc mh) {
	      monhocService.updateMonHoc(mmh, mh);
	  }

	  @RequestMapping(value="/monhoc/{mmh}", method=RequestMethod.PUT)
	  public void createMonHoc(@PathVariable String mmh, @RequestBody MonHoc mh) {
	      monhocService.createMonHoc(mmh, mh);
	  }

	  @RequestMapping(value="/monhoc/{mmh}", method=RequestMethod.PUT)
	  public void deleteMonHoc(@PathVariable String mmh, @RequestBody MonHoc mh) {
	      monhocService.deleteMonHoc(mmh, mh);
	  }
	  @RequestMapping(value = "/monhoc", method = RequestMethod.GET)
	  public List<Monhoc> getAllMonHoc() {
	    return monhocService.getMonHoc();
	  }

	  @RequestMapping(value = "/monhoc/search", method = RequestMethod.GET)
	  public List<MonHoc> searchByName(@RequestParam String name) {
	    return monhocService.findMonHocByName(name);
	  }
	 
	    
}
	 


	

}
