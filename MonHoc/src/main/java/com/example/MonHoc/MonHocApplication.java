package com.example.MonHoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonHocApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonHocApplication.class, args);
	}

}
